package chat_server;

import java.io.*;
import java.net.*;
import java.util.*;

public class server_frame extends javax.swing.JFrame 
{
   ArrayList clienteOutputStreams;
   ArrayList<String> usuarios;
   
   //Classe que irá ficar na thread 
   public class ClienteHandler implements Runnable	
   {
       BufferedReader reader;
       Socket socket;
       PrintWriter cliente;

       public ClienteHandler(Socket clienteSocket, PrintWriter cliente) 
       {
            this.cliente = cliente;
            try 
            {
                socket = clienteSocket;
                InputStreamReader isReader = new InputStreamReader(socket.getInputStream());
                reader = new BufferedReader(isReader);
            }
            catch (Exception ex) 
            {
                ta_chat.append("Erro não esperado... \n");
            }

       }

       @Override
       public void run() 
       {
            String mensagem, conectar = "conectar", disconectar = "disconectar", bate_papo = "batepapo" ;
            String[] data;

            try 
            {
                //MENSAGEM = "USUARIO:MENSAGEM:TIPODAMENSAGEM"
                while ((mensagem = reader.readLine()) != null) 
                {
                    ta_chat.append("Recebido: " + mensagem + "\n");
                    data = mensagem.split(":");
                   
                    if (data[2].equals(conectar)) 
                    {
                        broadcast((data[0] + ":" + data[1] + ":" + bate_papo));
                        addUsuario(data[0]);
                    } 
                    else if (data[2].equals(disconectar)) 
                    {
                        broadcast((data[0] + ": desconectou-se." + ":" + bate_papo));
                        userRemove(data[0]);
                    } 
                    else if (data[2].equals(bate_papo)) 
                    {
                        broadcast(mensagem);
                    } 
                    else 
                    {
                        ta_chat.append("Sem condições de realizar a comunicação. \n");
                    }
                } 
             } 
             catch (Exception ex) 
             {
                ta_chat.append("Conexão Perdida. \n");
                ex.printStackTrace();
                clienteOutputStreams.remove(cliente);
             } 
	} 
    }
   
   //Inciia os componentes do JFrame
    public server_frame() 
    {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        ta_chat = new javax.swing.JTextArea();
        b_start = new javax.swing.JButton();
        b_end = new javax.swing.JButton();
        b_users = new javax.swing.JButton();
        b_clear = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ChatPoli - Server - Projeto de Redes 1");
        setName("server"); // NOI18N
        setResizable(false);

        ta_chat.setColumns(20);
        ta_chat.setRows(5);
        jScrollPane1.setViewportView(ta_chat);

        b_start.setText("Iniciar");
        b_start.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_startActionPerformed(evt);
            }
        });

        b_end.setText("Encerrar");
        b_end.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_endActionPerformed(evt);
            }
        });

        b_users.setText("Usuários Online");
        b_users.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_usersActionPerformed(evt);
            }
        });

        b_clear.setText("Limpar Tela");
        b_clear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_clearActionPerformed(evt);
            }
        });

        jLabel1.setText("Rede de Computadores 1 -  POLI - UPE");

        jLabel2.setText("Grupo: Arthur Pedro, Breno Medeiros, Heitor Lima e Kalécio Pereira");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(b_end, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(b_start, javax.swing.GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(b_clear, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(b_users, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(b_start)
                    .addComponent(b_users))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(b_clear)
                    .addComponent(b_end))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void b_endActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_endActionPerformed
        broadcast("Server: está desligando, todas as conversas serão desconectadas!.\n:Chat");
        ta_chat.append("Server parando... \n");
        
        ta_chat.setText("");
        try 
        {
            Thread.sleep(5000);                 //5000 milisegundos = 5s
        } 
        catch(InterruptedException ex) {Thread.currentThread().interrupt();}
        
        
    }//GEN-LAST:event_b_endActionPerformed

    private void b_startActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_startActionPerformed

        ta_chat.append("Server iniciando...\n");
        Thread starter = new Thread(new ServerStart());
        starter.start();
        ta_chat.append("Server iniciado!\n");

    }//GEN-LAST:event_b_startActionPerformed

    private void b_usersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_usersActionPerformed
        //Exibe todos os usuarios cadastrados, que estão no ArrayList de OutputStream
        ta_chat.append("\n Usuários Online: \n");
        for(String usuario : usuarios)
        {
            ta_chat.append(usuario+"\n");
        }    
    }//GEN-LAST:event_b_usersActionPerformed

    private void b_clearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_clearActionPerformed
        //Limpa a tela
        ta_chat.setText("");
    }//GEN-LAST:event_b_clearActionPerformed

    public static void main(String args[]) 
    {
        java.awt.EventQueue.invokeLater(new Runnable() 
        {
            @Override
            public void run() {
                new server_frame().setVisible(true);
            }
        });
    }
    
    public class ServerStart implements Runnable 
    {
        @Override
        public void run() 
        {
            clienteOutputStreams = new ArrayList();
            usuarios = new ArrayList();  

            try 
            {
                ServerSocket serverSocket = new ServerSocket(2222);//Defini o socket e a porta do servidor

                while (true) 
                {
				Socket clienteSocket = serverSocket.accept();//Aceita o recebimento de pacotes
				PrintWriter writer = new PrintWriter(clienteSocket.getOutputStream());
				clienteOutputStreams.add(writer);

				Thread listener = new Thread(new ClienteHandler(clienteSocket, writer));
				listener.start();
				ta_chat.append("Recebeu uma conexão. \n");
                }
            }
            catch (Exception ex)
            {
                ta_chat.append("Erro ao fazer uma conexão. \n");
            }
        }
    }
    
    public void addUsuario (String data) 
    {
        //Adiciona um usuário ao ArrayList de usuários, afim de ter uma lista de
        //todos usuários on-line
        String message, add = ": :conectar", ok = "Server: :ok", nome = data;
        usuarios.add(nome);
        ta_chat.append(nome + " adicionado. \n");
        String[] tempList = new String[(usuarios.size())];
        usuarios.toArray(tempList);

        for (String token:tempList) 
        {
            message = (token + add);
            broadcast(message);
        }
        broadcast(ok);
    }
    
    public void userRemove (String data) 
    {
        String message, add = ": :conectar", ok = "Server: :ok", nome = data;
        usuarios.remove(nome);
        String[] tempList = new String[(usuarios.size())];
        usuarios.toArray(tempList);

        for (String token:tempList) 
        {
            message = (token + add);
            broadcast(message);
        }
        broadcast(ok);
    }
    
    //Função responsável por distribuir a comunicação para todos do bate-papo.
    public void broadcast(String mensagem) 
    {
	Iterator it = clienteOutputStreams.iterator();

        while (it.hasNext()) 
        {
            try 
            {
                PrintWriter writer = (PrintWriter) it.next();
		writer.println(mensagem);
		ta_chat.append("Enviando: " + mensagem + ", para "+writer.toString()+"\n");
                writer.flush();
                ta_chat.setCaretPosition(ta_chat.getDocument().getLength());
            } 
            catch (Exception ex) 
            {
		ta_chat.append("Erro no broadcast. \n");
            }
        } 
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_clear;
    private javax.swing.JButton b_end;
    private javax.swing.JButton b_start;
    private javax.swing.JButton b_users;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea ta_chat;
    // End of variables declaration//GEN-END:variables
}
